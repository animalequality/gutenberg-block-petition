I18N_DOMAIN := ae-block-petition
SUBDIR := languages
SOURCES := src
DIST := build
FILES := $(shell find $(SOURCES) -name block.js | sed -r "s!$(SOURCES)/(.*)!$(SOURCES)/\1 $(DIST)/\1!" | tr "\n" " ")

i18n-maps.json:
	node trace-imports.js $(FILES) >| $@

$(SUBDIR): i18n-maps.json ;
	$(MAKE) -C $@

$(SUBDIR)/$(I18N_DOMAIN).pot:
	wp i18n make-pot --domain=$(I18N_DOMAIN) --skip-block-json --exclude=$(DIST) ./ $@


all: $(SUBDIR) $(SUBDIR)/$(I18N_DOMAIN).pot i18n-maps.json
.PHONY: all $(SUBDIR)
