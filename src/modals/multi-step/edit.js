import {Fragment} from '@wordpress/element';
import {InspectorControls, useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import {Panel, PanelBody, PanelRow, SelectControl, CheckboxControl} from '@wordpress/components';

import modalTypes from '../../components/modal/types.js';
import {firstCharacterUppercase} from '../../utils/text.js';

export default ({attributes: {trigger, enableNextStepButton}, setAttributes}) => {
  return (
    <Fragment>
      <InspectorControls>
        <Panel>
          <PanelBody title='Modal Settings' initialOpen={true}>
            <PanelRow>
              <SelectControl
                label='Trigger'
                help='Choose the event type for which this popup should be used.'
                value={trigger}
                options={modalTypes}
                onChange={trigger => setAttributes({trigger})}
              />
            </PanelRow>
            <PanelRow>
              <CheckboxControl
                label='Enable Next Step Button'
                help='By default users can only proceed to the next step by using the CTA (e.g. share link). This option adds are forward button (similar to the back button)'
                checked={enableNextStepButton}
                onChange={enableNextStepButton => setAttributes({enableNextStepButton})}
              />
            </PanelRow>
          </PanelBody>
        </Panel>
      </InspectorControls>

      <div {...useBlockProps()}>
        <Panel>
          <PanelBody title={`${firstCharacterUppercase(trigger)} Modal (Multistep)`} initialOpen={true}>
            <PanelRow>
              <InnerBlocks
                allowedBlocks={[
                  'ae-petition/modal-multi-step-share',
                  'ae-petition/modal-multi-step-donate',
                  'ae-petition/modal-multi-step-generic'
                ]}
                template={[
                  ['ae-petition/modal-multi-step-share'],
                  ['ae-petition/modal-multi-step-donate'],
                  ['ae-petition/modal-multi-step-generic']
                ]}
              />
            </PanelRow>
          </PanelBody>
        </Panel>
      </div>
    </Fragment>
  );
};
