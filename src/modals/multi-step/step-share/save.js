import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';

export default () => (
  <div {...useBlockProps.save({className: 'carousel-item'})}>
    <InnerBlocks.Content />
  </div>
);
