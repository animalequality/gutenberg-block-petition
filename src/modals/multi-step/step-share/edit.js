import {InnerBlocks, useBlockProps} from '@wordpress/block-editor';
import {Panel, PanelRow, PanelBody} from '@wordpress/components';
import {useSelect} from '@wordpress/data';
import {_x} from '@wordpress/i18n';
import {getBlockIndexOfSibling, getMultiStepTemplate} from '../../../utils/blocks.js';

export default ({clientId}) => {
  const stepIndex = useSelect(select => {
    return getBlockIndexOfSibling(select, clientId);
  });

  const shareColumnTemplate = type => [
    'core/column', {allowedBlocks: ['animalequality/share']}, [
      ['animalequality/share', {type}]
    ]
  ];

  return <div {...useBlockProps()}>
    <Panel>
      <PanelBody title={`Step ${stepIndex}`} initialOpen={true}>
        <PanelRow>
          <InnerBlocks
            allowedBlocks={['core/columns']}
            template={
              getMultiStepTemplate([
                [
                  'core/paragraph', {
                    content: 'This is example content'
                  }
                ], [
                  'core/paragraph', {
                    content: '<strong>Share this campaign</strong>'
                  }
                ], [
                  'core/columns', {
                    isStackedOnMobile: false
                  }, [
                    shareColumnTemplate('whatsapp'),
                    shareColumnTemplate('twitter'),
                    shareColumnTemplate('facebook')
                  ]
                ]
              ], ['core/heading', 'core/paragraph'])
            }
          />
        </PanelRow>
      </PanelBody>
    </Panel>
  </div>;
};
