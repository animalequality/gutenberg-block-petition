import {InnerBlocks, useBlockProps} from '@wordpress/block-editor';
import {Panel, PanelRow, PanelBody} from '@wordpress/components';
import {useSelect} from '@wordpress/data';
import {getBlockIndexOfSibling, getMultiStepTemplate} from '../../../utils/blocks.js';

export default ({clientId}) => {
  const stepIndex = useSelect(select => {
    return getBlockIndexOfSibling(select, clientId);
  });

  return <div {...useBlockProps()}>
    <Panel>
      <PanelBody title={`Step ${stepIndex}`} initialOpen={true}>
        <PanelRow>
          <InnerBlocks
            allowedBlocks={['core/columns']}
            template={
              getMultiStepTemplate([
                ['animalequality/mini-donation-form', {
                  targetBlank: true
                }]
              ], ['core/heading', 'core/paragraph', 'animalequality/mini-donation-form'])
            }
          />
        </PanelRow>
      </PanelBody>
    </Panel>
  </div>;
};
