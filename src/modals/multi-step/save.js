import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import Modal from '../../components/modal/modal.js';

export default ({attributes: {trigger, enableNextStepButton}}) => (
  <div {...useBlockProps.save()} {...(enableNextStepButton && {'data-enable-next-step-button': 'true'})}>
    <Modal className={`petition-modal-${trigger}`}>
      <div className="carousel slide">
        <div className="carousel-inner">
          <InnerBlocks.Content />
        </div>
      </div>
    </Modal>
  </div>
);
