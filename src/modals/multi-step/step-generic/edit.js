import {InnerBlocks, useBlockProps} from '@wordpress/block-editor';
import {Panel, PanelRow, PanelBody} from '@wordpress/components';
import {useSelect} from '@wordpress/data';
import {getBlockIndexOfSibling, getMultiStepTemplate} from '../../../utils/blocks.js';

export default ({clientId}) => {
  const stepIndex = useSelect(select => {
    return getBlockIndexOfSibling(select, clientId);
  });

  return <div {...useBlockProps()}>
    <Panel>
      <PanelBody title={`Step ${stepIndex}`} initialOpen={true}>
        <PanelRow>
          <InnerBlocks
            allowedBlocks={['core/columns']}
            template={
              getMultiStepTemplate([
                [
                  'core/paragraph', {
                    content: 'This is example content'
                  }
                ], [
                  'core/buttons', {}, [
                    ['core/button', {
                      text: 'Call To Action',
                      url: 'https://example.com',
                      backgroundColor: 'primary'
                    }]
                  ]
                ]
              ], ['core/heading', 'core/paragraph', 'core/image', 'core/buttons'])
            }
          />
        </PanelRow>
      </PanelBody>
    </Panel>
  </div>;
};
