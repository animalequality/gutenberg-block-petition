import ready from 'document-ready';
import Carousel from 'bootstrap/js/dist/carousel.js';
import {event as gtmEvent} from '../../utils/tracking.js';

const navigationIcon = `<svg version="1.1" viewBox="0 0 1012 1012" xmlns="http://www.w3.org/2000/svg" fill="#fff">
  <path d="m463.87 273.98h-76.8c-75.51 77.36-150.93 154.63-226.44 232 75.56 77.41 150.96 154.67 226.44 232h76.79c0.07-0.06 0.13-0.13 0.19-0.19-66.37-68-132.58-135.84-198.92-203.81h586.22v-56h-586.22c66.36-67.99 132.6-135.86 198.93-203.81-0.06-0.06-0.12-0.13-0.19-0.19z"/>
  <path d="m505.99 1012c-68.3 0-134.57-13.38-196.96-39.77-60.26-25.49-114.37-61.97-160.83-108.43-46.47-46.46-82.95-100.58-108.43-160.83-26.39-62.39-39.77-128.66-39.77-196.96s13.38-134.57 39.77-196.96c25.49-60.26 61.97-114.37 108.43-160.83 46.46-46.47 100.58-82.95 160.83-108.43 62.39-26.39 128.66-39.77 196.96-39.77s134.57 13.38 196.96 39.77c60.26 25.49 114.37 61.97 160.83 108.43 46.47 46.47 82.95 100.58 108.43 160.83 26.39 62.39 39.77 128.66 39.77 196.96s-13.38 134.57-39.77 196.96c-25.49 60.26-61.97 114.37-108.43 160.83-46.46 46.47-100.58 82.95-160.83 108.43-62.39 26.39-128.66 39.77-196.96 39.77zm0-983.68c-64.49 0-127.05 12.63-185.93 37.53-56.88 24.06-107.97 58.5-151.84 102.38-43.87 43.87-78.32 94.96-102.38 151.84-24.9 58.88-37.53 121.44-37.53 185.93s12.63 127.05 37.53 185.93c24.06 56.88 58.5 107.97 102.38 151.84 43.87 43.87 94.96 78.32 151.84 102.38 58.88 24.9 121.44 37.53 185.93 37.53s127.05-12.63 185.93-37.53c56.88-24.06 107.97-58.5 151.84-102.38 43.87-43.87 78.32-94.96 102.38-151.84 24.9-58.88 37.53-121.44 37.53-185.93s-12.63-127.05-37.53-185.93c-24.06-56.88-58.5-107.97-102.38-151.84-43.87-43.87-94.96-78.32-151.84-102.38-58.88-24.9-121.44-37.53-185.93-37.53z"/>
</svg>`;

const createNavigationButton = (parent, direction, clickCallback, clickable) => {
  const buttonElement = document.createElement('div');

  buttonElement.classList.add('modal-multi-step-navigation__button');
  buttonElement.classList.add(`modal-multi-step-navigation__button--${direction}`);

  if (clickable) {
    buttonElement.innerHTML = navigationIcon;
    buttonElement.addEventListener('click', clickCallback);
  }

  parent.appendChild(buttonElement);
};

ready(() => {
  const modals = document.querySelectorAll('.wp-block-ae-petition-modal-multi-step');

  modals.forEach(modal => {
    const carouselElement = modal.querySelector('.carousel');
    const carouselItemElements = carouselElement && modal.querySelectorAll('.carousel-item');
    const enableNextStepButton = modal.dataset.enableNextStepButton === 'true';

    if (carouselItemElements.length) {
      carouselItemElements[0].classList.add('active');

      let currentSlide = 0;
      const slideTriggerElements = modal.querySelectorAll('.carousel-item a');
      const carousel = new Carousel(carouselElement, {
        interval: false,
        wrap: false,
        touch: false
      });

      modal.addEventListener('shown.bs.modal', () => {
        carousel.to(0);
        currentSlide = 0;
      });

      modal.addEventListener('hide.bs.modal', () => {
        gtmEvent('petition:modal:close', {step: currentSlide});
      });

      carouselElement.addEventListener('slid.bs.carousel', event => {
        currentSlide = event.to + 1;
        gtmEvent('petition:modal:slide', {from: event.from + 1, to: event.to + 1});
      });

      carouselItemElements.forEach((element, index) => {
        const navigationElement = document.createElement('div');
        const shouldRenderPreviousNavigationButton = index > 0;
        const shouldRenderNextNavigationButton = index < carouselItemElements.length - 1 && enableNextStepButton;

        if (shouldRenderPreviousNavigationButton || shouldRenderNextNavigationButton) {
          navigationElement.classList.add('modal-multi-step-navigation');

          element.querySelector('.wp-block-column > .wp-block-group').appendChild(navigationElement);

          createNavigationButton(navigationElement, 'previous', carousel.prev.bind(carousel), shouldRenderPreviousNavigationButton);
          createNavigationButton(navigationElement, 'next', carousel.next.bind(carousel), shouldRenderNextNavigationButton);
        }
      });

      slideTriggerElements.forEach(element => element.addEventListener('click', carousel.next.bind(carousel)));
    }
  });
});
