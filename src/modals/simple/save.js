import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import Modal from '../../components/modal/modal.js';

export default ({attributes: {trigger}}) => (
  <div {...useBlockProps.save()}>
    <Modal className={`petition-modal-${trigger}`}>
      <InnerBlocks.Content />
    </Modal>
  </div>
);
