import {registerBlockType} from '@wordpress/blocks';

import edit from './edit.js';
import save from './save.js';
import icon from '../../../block-icon.js';
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata, {
  icon,
  edit,
  save
});
