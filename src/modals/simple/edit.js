import {Fragment} from '@wordpress/element';
import {InspectorControls, useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import {Panel, PanelRow, PanelBody, SelectControl} from '@wordpress/components';

import modalTypes from '../../components/modal/types.js';
import {firstCharacterUppercase} from '../../utils/text.js';

export default ({attributes: {trigger}, setAttributes}) => {
  return (
    <Fragment>
      <InspectorControls>
        <Panel>
          <PanelBody title='Modal Settings' initialOpen={true}>
            <PanelRow>
              <SelectControl
                label='Trigger'
                help='Choose which event should trigger this popup.'
                value={trigger}
                options={modalTypes}
                onChange={trigger => setAttributes({trigger})}
              />
            </PanelRow>
          </PanelBody>
        </Panel>
      </InspectorControls>
      <div {...useBlockProps()}>
        <Panel>
          <PanelBody title={`${firstCharacterUppercase(trigger)} Modal (Simple)`} initialOpen={true}>
            <PanelRow>
              <InnerBlocks allowedBlocks={['core/paragraph', 'core/image', 'core/buttons', 'core/button', 'core/list']} />
            </PanelRow>
          </PanelBody>
        </Panel>
      </div>
    </Fragment>
  );
};
