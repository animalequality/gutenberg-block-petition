import {Fragment, useState, useRef, useEffect} from '@wordpress/element';
import {InspectorControls, useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import {Panel, PanelBody, PanelRow, SelectControl, TextControl, CheckboxControl, __experimentalText as Text} from '@wordpress/components';
import {_x} from '@wordpress/i18n';
import {find} from 'lodash';
import {CYGNE_URL, getCountryList, isSystemUsingCygne, isSystemUsingOwnForm} from '../utils/config.js';
import {recaptchaSiteKey} from '../utils/form.js';
import textFieldVariations from './row/fields/text/variations.js';
import checkboxFieldVariations from './row/fields/checkbox/variations.js';
import selectFieldVariations from './row/fields/select/variations.js';
import emailFieldVariations from './row/fields/email/variations.js';

export default ({attributes: {country, system, petitionId, petitionSlug, pardotEndpoint, thankYouPageUrl, preventAutofill, enableRecaptcha, recaptchaPosition, messageVariant, utmSource, utmMedium, utmCampaign, utmContent, utmTerm, utmTag}, setAttributes}) => {
  const [cygnePetitionName, setCygnePetitionName] = useState('');
  const [cygnePetitionRequiredFields, setCygnePetitionRequiredFields] = useState('');
  const [hasPetitionInfoError, setHasPetitionInfoError] = useState(false);
  const petitionIdChangeTimeout = useRef(null);

  const petitionIdUpate = async(petitionId, system) => {
    window.clearTimeout(petitionIdChangeTimeout.current);
    setAttributes({petitionId});

    if (!isSystemUsingCygne(system)) {
      setCygnePetitionName(null);
      setCygnePetitionRequiredFields(null);
      setHasPetitionInfoError(false);
      return;
    }

    petitionIdChangeTimeout.current = window.setTimeout(async() => {
      try {
        const response = await fetch(CYGNE_URL.replace(/%d.*/, petitionId));
        const petitionDetails = await response.json();

        if (petitionDetails) {
          setHasPetitionInfoError(false);
          setCygnePetitionName(petitionDetails.title);
          setCygnePetitionRequiredFields(petitionDetails.requirements.join(', '));
          setAttributes({
            petitionSlug: petitionDetails.slug || '',
            enableRecaptcha: Array.isArray(petitionDetails.requirements) && petitionDetails.requirements.includes('recaptcha')
          });
        }
      } catch (error) {
        setCygnePetitionName(null);
        setCygnePetitionRequiredFields(null);
        setHasPetitionInfoError(true);
      }
    }, 1000);
  };

  const getPetitionIdLabel = () => {
    if (system === 'url') {
      return 'Petition URL';
    } else {
      return 'Petition ID';
    }
  };

  useEffect(() => {
    petitionIdUpate(petitionId, system);
  }, []);

  return (
    <Fragment>
      <InspectorControls>
        <Panel>
          <PanelBody title='Petition Settings' initialOpen={true}>
            <PanelRow>
              <SelectControl
                label='Country'
                value={country}
                options={getCountryList()}
                onChange={country => setAttributes({country})}
              />
            </PanelRow>
            <PanelRow>
              <SelectControl
                label='System'
                value={system}
                help='Cygne is the system used for most countries. In case of doubt, contact the TECH team.'
                options={[
                  {label: 'Cygne', value: 'cygne'},
                  {label: 'Cygne + Pardot', value: 'cygne_pardot'},
                  {label: 'Salesforce', value: 'salesforce'},
                  {label: 'Everyaction', value: 'everyaction'},
                  {label: 'URL', value: 'url'}
                ]}
                onChange={system => {
                  setAttributes({system});
                  petitionIdUpate(petitionId, system);
                }}
              />
            </PanelRow>
            {
              system === 'url' &&
              <PanelRow>
                <Text color='red'>
                  Providing the URL should only be used exceptionally. Consider asking the TECH team to implement proper support.
                </Text>
              </PanelRow>
            }
            <PanelRow>
              <TextControl
                label={getPetitionIdLabel()}
                value={petitionId}
                help='For Cygne you get the ID from the TECH team after requesting a new petition. For EveryAction the ID can be found in the backend when creating the form. For Salesforce, this is the campaign ID from salesforce'
                onChange={async petitionId => petitionIdUpate(petitionId, system)}
              />
            </PanelRow>
            {
              hasPetitionInfoError &&
              <PanelRow>
                <Text color='red'>
                  Petition ID not found or error loading the settings. Insert ID again or change settings manually
                </Text>
              </PanelRow>
            }
            {
              !hasPetitionInfoError && cygnePetitionName &&
              <PanelRow>
                <Text color='green'>
                  {`Petition name: ${cygnePetitionName}`}<br />
                  {`Required fields: ${cygnePetitionRequiredFields}`}
                </Text>
              </PanelRow>
            }
            {
              isSystemUsingOwnForm(system) &&
              <PanelRow>
                <TextControl
                  label='Petition Slug'
                  value={petitionSlug}
                  help='This is used to identify the petition e.g. for webtracking. In case of cygne it will be prefilled with the information of the petition system.'
                  onChange={petitionSlug => setAttributes({petitionSlug})}
                />
              </PanelRow>
            }
            {
              system === 'cygne_pardot' &&
              <PanelRow>
                <TextControl
                  label='Pardot Form Handler'
                  value={pardotEndpoint}
                  onChange={pardotEndpoint => setAttributes({pardotEndpoint})}
                />
              </PanelRow>
            }
          </PanelBody>
        </Panel>
        <Panel>
          <PanelBody title='Miscellaneous' initialOpen={true}>
            {
              isSystemUsingOwnForm(system) &&
              <Fragment>
                <PanelRow>
                  <CheckboxControl
                    label='Prevent Autofill'
                    help='Once the users submits a petition successfully, the form data is stored in the users browser. By default this information will be restored an all AE petitions in the future. Check this if you do not want that to happen.'
                    checked={preventAutofill}
                    onChange={preventAutofill => setAttributes({preventAutofill})}
                  />
                </PanelRow>
                <PanelRow>
                  <CheckboxControl
                    label='Enable Recaptcha'
                    help='Enables the invisible google recaptcha for this petition.'
                    checked={enableRecaptcha && recaptchaSiteKey()}
                    disabled={!recaptchaSiteKey()}
                    onChange={enableRecaptcha => setAttributes({enableRecaptcha})}
                  />
                </PanelRow>
                <PanelRow>
                  <SelectControl
                    label='Recaptcha Badge position'
                    value={recaptchaPosition}
                    help='Although the recaptcha is considered "invisible" there is still a badge, informing people that the page is protected by recaptcha. Default postition is bottom right.'
                    options={[
                      {label: 'Bottom Right', value: ''},
                      {label: 'Bottom Left', value: 'bottomleft'}
                    ]}
                    onChange={recaptchaPosition => setAttributes({recaptchaPosition})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='Thank You Page URL'
                    value={thankYouPageUrl}
                    help='If provided, the user will be redirected to that page after successfully signing the petition instead of being presented with the success modal'
                    onChange={thankYouPageUrl => setAttributes({thankYouPageUrl})}
                  />
                </PanelRow>
              </Fragment>
            }
            {
              isSystemUsingCygne(system) &&
              <PanelRow>
                <TextControl
                  label='Message Variant'
                  help='Different thank you email variants can be configured with a cygne petition. Provide the identifier here if another than the default one should be used for this petition block'
                  value={messageVariant}
                  onChange={messageVariant => setAttributes({messageVariant})}
                />
              </PanelRow>
            }
          </PanelBody>
        </Panel>
        <Panel>
          <PanelBody title='Tracking' initialOpen={false}>
            {
              isSystemUsingOwnForm(system) &&
              <Fragment>
                <PanelRow>
                  <TextControl
                    label='UTM Source'
                    value={utmSource}
                    help='This value will be used if not defined in the URL'
                    onChange={utmSource => setAttributes({utmSource})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='UTM Medium'
                    value={utmMedium}
                    help='This value will be used if not defined in the URL'
                    onChange={utmMedium => setAttributes({utmMedium})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='UTM Campaign'
                    value={utmCampaign}
                    help='This value will be used if not defined in the URL'
                    onChange={utmCampaign => setAttributes({utmCampaign})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='UTM Term'
                    value={utmTerm}
                    help='This value will be used if not defined in the URL'
                    onChange={utmTerm => setAttributes({utmTerm})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='UTM Content'
                    value={utmContent}
                    help='This value will be used if not defined in the URL'
                    onChange={utmContent => setAttributes({utmContent})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='UTM Tag'
                    value={utmTag}
                    help='IMPORTANT: UTM Tag is not part of the official tracking parameters. This value will be used if not defined in the URL'
                    onChange={utmTag => setAttributes({utmTag})}
                  />
                </PanelRow>
              </Fragment>
            }
          </PanelBody>
        </Panel>

      </InspectorControls>

      <div {...useBlockProps()}>
        <form>
          {
            isSystemUsingOwnForm(system) &&
            <InnerBlocks
              allowedBlocks={['ae-petition/form-row', 'core/paragraph']}
              template={[
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-text', {
                    name: find(textFieldVariations, {name: 'firstname'}).attributes.name,
                    label: find(textFieldVariations, {name: 'firstname'}).attributes.label
                  }],
                  ['ae-petition/field-text', {
                    name: find(textFieldVariations, {name: 'lastname'}).attributes.name,
                    label: find(textFieldVariations, {name: 'lastname'}).attributes.label
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-email', {
                    name: find(emailFieldVariations, {name: 'email'}).attributes.name,
                    label: find(emailFieldVariations, {name: 'email'}).attributes.label
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-select', {
                    name: find(selectFieldVariations, {name: 'country'}).attributes.name,
                    label: find(selectFieldVariations, {name: 'country'}).attributes.label
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-text', {
                    name: find(textFieldVariations, {name: 'postalcode'}).attributes.name,
                    label: find(textFieldVariations, {name: 'postalcode'}).attributes.label
                  }],
                  ['ae-petition/field-text', {
                    name: find(textFieldVariations, {name: 'city'}).attributes.name,
                    label: find(textFieldVariations, {name: 'city'}).attributes.label
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-checkbox', {
                    name: find(checkboxFieldVariations, {name: 'newsletter'}).attributes.name,
                    label: find(checkboxFieldVariations, {name: 'newsletter'}).attributes.label,
                    required: false
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-checkbox', {
                    name: find(checkboxFieldVariations, {name: 'privacy'}).attributes.name,
                    label: find(checkboxFieldVariations, {name: 'privacy'}).attributes.label
                  }]
                ]],
                ['ae-petition/form-row', {}, [
                  ['ae-petition/field-submit', {label: _x('Sign', 'ae-block-petition')}]
                ]]
              ]}
            />
          }
          {
            !isSystemUsingOwnForm(system) &&
            <p>
              An external form is being used, therefore the form needs to be configured externally.
            </p>
          }
        </form>
      </div>
    </Fragment>
  );
};
