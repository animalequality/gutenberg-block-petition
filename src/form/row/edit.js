import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';

export default () => {
  const fieldBlocks = [
    'text',
    'checkbox',
    'select',
    'submit',
    'tel',
    'email',
    'google'
  ];

  return (
    <div {...useBlockProps()}>
      <InnerBlocks allowedBlocks={fieldBlocks.map(block => `ae-petition/field-${block}`)} />
    </div>
  );
};
