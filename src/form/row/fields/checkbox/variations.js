import {_x} from '@wordpress/i18n';
import {createVariation} from '../../../../utils/fields.js';

export default [
  createVariation('checkbox', _x('Checkbox', 'default field label', 'ae-block-petition'), 'Generic checkbox', true),
  createVariation('newsletter', _x('Newsletter', 'default field label', 'ae-block-petition'), 'Petition newsletter checkbox'),
  createVariation('animaldefenders', _x('Animal Defenders', 'default field label', 'ae-block-petition'), 'Petition animal defenders checkbox'),
  createVariation('privacy', _x('Privacy', 'default field label', 'ae-block-petition'), 'Petition privacy consent checkbox')
];
