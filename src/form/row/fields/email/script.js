import ready from 'document-ready';
import {validate as validateEmail} from '../../../../utils/emailValidation.js';

ready(() => {
  const emailFields = document.querySelectorAll('.wp-block-ae-petition-petition input[type=email]');

  emailFields.forEach(field => {
    field.addEventListener('change', () => {
      validateEmail(field);
    });
  });
});
