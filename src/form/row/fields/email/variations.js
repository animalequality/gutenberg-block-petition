import {_x} from '@wordpress/i18n';
import {createVariation} from '../../../../utils/fields.js';

export default [
  createVariation('emailGeneric', _x('Email', 'default field label', 'ae-block-petition'), 'Generic email field', true),
  createVariation('email', _x('Email', 'default field label', 'ae-block-petition'), 'Petition email field')
];
