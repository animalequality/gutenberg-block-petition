import {_x} from '@wordpress/i18n';
import {createVariation} from '../../../../utils/fields.js';

export default [
  createVariation('tel', _x('Phone', 'default field label', 'ae-block-petition'), 'Generic tel field', true),
  createVariation('phone', _x('Phone', 'default field label', 'ae-block-petition'), 'Petition tel field')
];
