import {registerBlockType} from '@wordpress/blocks';
import variations from './variations.js';

import edit from '../../../../components/field/edit.js';
import save from '../../../../components/field/save.js';
import icon from '../../../../../block-icon.js';
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata, {
  icon,
  edit: edit('tel'),
  save: save('tel'),
  variations
});
