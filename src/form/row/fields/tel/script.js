import ready from 'document-ready';
import initPhoneFields from '@animalequality/address-form-tools/src/telephone.js';

ready(() => {
  const petitionBlocks = document.querySelectorAll('.wp-block-ae-petition-petition');

  petitionBlocks.forEach(block => {
    const telFields = block.querySelectorAll('input[type=tel]');
    const countryField = block.querySelector('select[name=country]');

    if (countryField) {
      telFields.forEach(field => {
        initPhoneFields(field, countryField);
      });
    }
  });
});
