import {_x} from '@wordpress/i18n';
import {createVariation} from '../../../../utils/fields.js';

export default [
  createVariation('text', _x('Text', 'default field label', 'ae-block-petition'), 'Generic text field', true),
  createVariation('firstname', _x('First Name', 'default field label', 'ae-block-petition'), 'Petition firstname field'),
  createVariation('lastname', _x('Last Name', 'default field label', 'ae-block-petition'), 'Petition lastname field'),
  createVariation('street', _x('Street', 'default field label', 'ae-block-petition'), 'Petition street field'),
  createVariation('city', _x('City', 'default field label', 'ae-block-petition'), 'Petition city field'),
  createVariation('postalcode', _x('Postal Code', 'default field label', 'ae-block-petition'), 'Petition postalcode field')
];
