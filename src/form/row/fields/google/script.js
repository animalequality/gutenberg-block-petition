import ready from 'document-ready';
import googleSigning from '../../../../utils/google.js';

ready(() => {
  window.aeBlockPetitionHandleGoogleSigning = googleSigning;
});
