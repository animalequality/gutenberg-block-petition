import {useBlockProps} from '@wordpress/block-editor';
import {renderGoogleConnect} from './utils.js';

export default () => {
  return renderGoogleConnect(useBlockProps.save());
};
