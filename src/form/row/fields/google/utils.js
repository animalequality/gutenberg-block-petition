import {_x} from '@wordpress/i18n';

const renderGoogleConnect = blockProps => {
  return window.aeBlockPetitionConfig && window.aeBlockPetitionConfig.googleSigningClientId
    ? <div {...blockProps}>
      <script src="https://accounts.google.com/gsi/client" defer></script>
      <div id="g_id_onload" data-client_id={window.aeBlockPetitionConfig.googleSigningClientId} data-context="use" data-callback="aeBlockPetitionHandleGoogleSigning" data-auto_select="true"></div>
      <div className="g_id_signin" data-type="standard" data-size="large" data-theme="outline" data-text={_x('Sign in with Google', 'button label for google login', 'ae-block-petition')} data-shape="rectangular" data-logo_alignment="left"></div>
    </div>
    : _x('Google client ID is missing', 'error message if config is missing for google login', 'ae-block-petition');
};

export {renderGoogleConnect};
