import {Fragment} from '@wordpress/element';
import {InspectorControls, useBlockProps} from '@wordpress/block-editor';
import {Panel, PanelBody, PanelRow, TextControl} from '@wordpress/components';

export default ({attributes: {label}, setAttributes}) => {
  return (
    <Fragment>
      <InspectorControls>
        <Panel>
          <PanelBody title='Field Settings' initialOpen={true}>
            <PanelRow>
              <TextControl
                label='Overwrite default label'
                value={label}
                onChange={label => setAttributes({label})}
              />
            </PanelRow>
          </PanelBody>
        </Panel>
      </InspectorControls>

      <button type="submit" {...useBlockProps()}>
        {label}
      </button>
    </Fragment>
  );
};
