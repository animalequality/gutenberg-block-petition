import {useBlockProps} from '@wordpress/block-editor';

export default ({attributes: {label}}) => {
  return <button type="submit" {...useBlockProps.save()}>
    {label}
  </button>;
};
