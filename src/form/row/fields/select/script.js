import ready from 'document-ready';
import {countryChangeHandler, postalCodeChangeHandler} from '@animalequality/address-form-tools/src/cp-country.js';

function locationHint(postalCodeField, geodata) {
  const hintClass = 'ae-petition-location-hint';
  const hintElement = postalCodeField.parentElement.querySelector(`.${hintClass}`);

  if (hintElement) {
    hintElement.remove();
  }

  if (geodata && geodata.admin1) {
    postalCodeField.insertAdjacentHTML('afterend', `<div class="${hintClass}">${geodata.admin1}</div>`);
  }
}

ready(() => {
  const petitionBlocks = document.querySelectorAll('.wp-block-ae-petition-petition');

  petitionBlocks.forEach(block => {
    const postalcodeField = block.querySelector('input[name=postalcode]');
    const cityField = block.querySelector('input[name=city]');
    const countryField = block.querySelector('select[name=country]');

    if (countryField && postalcodeField) {
      countryField.addEventListener('change', () => {
        countryChangeHandler(countryField, postalcodeField);
        locationHint(postalcodeField, null);
        postalcodeField.value = '';

        if (cityField) {
          cityField.value = '';
        }
      });

      postalcodeField.addEventListener('change', () => {
        postalCodeChangeHandler(countryField, postalcodeField);
        locationHint(postalcodeField, null);

        if (cityField) {
          cityField.value = '';
        }
      });

      document.addEventListener('address-form-tools:has-postal-code', (event) => {
        postalcodeField.placeholder = event.detail.show && 'fmt' in event.detail && event.detail.fmt ? event.detail.fmt : '';
      });

      document.addEventListener('geocode:geocoded', event => {
        const geodata = event.detail;

        locationHint(postalcodeField, geodata);

        if (cityField && geodata && !cityField.value && geodata.city) {
          cityField.value = geodata.city;
        }
      });
    }
  });
});
