import {_x} from '@wordpress/i18n';
import {createVariation} from '../../../../utils/fields.js';

export default [
  createVariation('select', _x('Select', 'default field label', 'ae-block-petition'), 'Generic select field', true),
  createVariation('country', _x('Country', 'default field label', 'ae-block-petition'), 'Petition country field')
];
