import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';

export default () => (
  <div {...useBlockProps.save()}>
    <InnerBlocks.Content />
  </div>
);
