import {Fragment} from '@wordpress/element';
import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import {_x} from '@wordpress/i18n';
import {getFormAction, isSystemUsingOwnForm} from '../utils/config.js';

const hideLoadingOverlay = `.wp-block-ae-petition-form .loading-overlay {
  display: none;
}`;

export default ({
  attributes: {
    country,
    system,
    petitionId,
    petitionSlug,
    pardotEndpoint,
    thankYouPageUrl,
    preventAutofill,
    enableRecaptcha,
    recaptchaPosition,
    messageVariant,
    utmSource,
    utmMedium,
    utmCampaign,
    utmTerm,
    utmContent,
    utmTag
  }
}) => (
  <div
    {...useBlockProps.save()}
    data-system={system}
    data-petition-id={petitionId}
    data-petition-slug={petitionSlug}
    data-prevent-autofill={preventAutofill}
    data-enable-recaptcha={enableRecaptcha}
    {...(recaptchaPosition && {'data-recaptcha-position': recaptchaPosition})}
    data-pardot-endpoint={pardotEndpoint}
    {...(thankYouPageUrl && {'data-thank-you-page-url': thankYouPageUrl})}
  >
    {
      isSystemUsingOwnForm(system) &&
      <Fragment>
        <div className='loading-overlay' />
        <noscript>
          <style>
            {hideLoadingOverlay}
          </style>
        </noscript>
        <form action={getFormAction(country, system, petitionId)} method='POST'>
          <Fragment>
            <InnerBlocks.Content />
            {
              window.aeBlockPetitionConfig.successRedirectUrl &&
              <input type="hidden" name="redirect_success" value={window.aeBlockPetitionConfig.successRedirectUrl} />
            }
            {
              window.aeBlockPetitionConfig.errorRedirectUrl &&
              <input type="hidden" name="redirect_error" value={window.aeBlockPetitionConfig.errorRedirectUrl} />
            }
            {
              utmContent &&
              <input type="hidden" name="utm_content" value={utmContent} />
            }
            {
              utmMedium &&
              <input type="hidden" name="utm_medium" value={utmMedium} />
            }
            {
              utmSource &&
              <input type="hidden" name="utm_source" value={utmSource} />
            }
            {
              utmTerm &&
              <input type="hidden" name="utm_term" value={utmTerm} />
            }
            {
              utmCampaign &&
              <input type="hidden" name="utm_campaign" value={utmCampaign} />
            }
            {
              utmTag &&
              <input type="hidden" name="utm_tag" value={utmTag} />
            }
          </Fragment>

          {
            messageVariant &&
            <input type='hidden' name='variant' value={messageVariant} />
          }
        </form>
      </Fragment>
    }
    {
      system === 'everyaction' &&
      <Fragment>
        <div
          className="ngp-form mt-3"
          data-form-url={petitionId.includes('https://') ? petitionId : `https://secure.everyaction.com/v2/forms/${petitionId}`}
          data-fastaction-endpoint="https://fastaction.ngpvan.com"
          data-inline-errors="true"
          data-fastaction-nologin="true"
          data-databag-endpoint="https://profile.ngpvan.com"
          data-databag="everybody"
          data-mobile-autofocus="false"
        />

        <link rel='preload' href='https://d3rse9xjbp8270.cloudfront.net/at.js' as='script' crossOrigin='anonymous' />
        <link rel='preload' href='https://d3rse9xjbp8270.cloudfront.net/at.min.css' as='style' />
        <script type='text/javascript' src='https://d3rse9xjbp8270.cloudfront.net/at.js' crossOrigin='anonymous'></script>

        <noscript>
          {_x('Please enable Javascript in your browser to use this form', 'ae-block-petition')}
        </noscript>
      </Fragment>
    }
  </div>
);
