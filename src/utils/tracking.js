function filterSensitiveData(data) {
  for (const key in data) {
    if (key.toLowerCase().includes('name') || key.toLowerCase().includes('mail')) {
      delete data[key];
    }
  }

  return data;
}

/**
 * Send generic google tag manager event
 *
 * @param eventName Name of the event to send
 * @param data Additional variables to be set
 */
const event = (eventName, data = {}) => {
  if (!eventName || !Array.isArray(window.dataLayer)) {
    return;
  }

  window.dataLayer.push({
    event: eventName,
    ...filterSensitiveData(data)
  });
};

export {event};
