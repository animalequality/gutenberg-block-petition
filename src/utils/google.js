import rsa from 'jsrsasign';

const CERT_URLS = 'https://www.googleapis.com/oauth2/v1/certs';
const JWT_FIELD_MAP = {
  email: 'email',
  family_name: 'lastname',
  given_name: 'firstname'
};

async function validate(creds, aud) {
  const parsed = rsa.KJUR.jws.JWS.parse(creds);
  const kid = parsed.headerObj.kid;
  const emailVerified = parsed.payloadObj.emailVerified;

  let keys = await fetch(CERT_URLS);
  keys = await keys.json();

  if (!emailVerified) {
    console.warn('googlejwt: email is not_verified');
    return false;
  }

  if (!(kid in keys)) {
    console.warn(`googlejwt: kid ${kid} not found`);
    return false;
  }

  return rsa.KJUR.jws.JWS.verifyJWT(creds, keys[kid], {
    alg: ['RS256'],
    iss: ['https://accounts.google.com'],
    aud: [aud],
    gracePeriod: 12 * 60 * 60
  });
}

// https://developers.google.com/identity/gsi/web/guides/handle-credential-responses-js-functions
export default async response => {
  const petitionBlocks = document.querySelectorAll('.wp-block-ae-petition-petition');

  try {
    petitionBlocks.forEach(async block => {
      const form = block.querySelector('.wp-block-ae-petition-form');
      const clientId = block.querySelector('div[id="g_id_onload"]').getAttribute('data-client_id');
      const isValid = await validate(response.credential, clientId);

      if (!isValid) {
        throw new Error('googlejwt: Invalid token');
      }

      const data = rsa.KJUR.jws.JWS.parse(response.credential);

      for (const [key, fieldName] of Object.entries(JWT_FIELD_MAP)) {
        const field = form.querySelector(`[name="${fieldName}"]`);

        if (field && data.payloadObj[key] !== undefined) {
          field.value = data.payloadObj[key];
        }
      }
    });
  } catch (error) {
    console.error('Error connecting to google: ' + error);
  }
};
