import {getUtmParameterObject} from '../utils/url.js';
import Cookies from 'js-cookie';
import {isTypeSignature, isTypeSubscription} from './config.js';

const filterFormData = (form, filterList = [], mapFieldNames = false) => {
  const formData = new FormData(form);

  filterList.forEach((filter) => {
    formData.delete(filter);
  });

  // Mapping because of different field names required by cygne and listo
  if (mapFieldNames) {
    const mapping = [];

    if (mapFieldNames === 'cygne') {
      mapping.push({from: 'firstname', to: 'firstName'});
      mapping.push({from: 'lastname', to: 'lastName'});
      mapping.push({from: 'postalcode', to: 'postalCode'});
    } else if (mapFieldNames === 'salesforce') {
      mapping.push({from: 'firstname', to: 'first_name'});
      mapping.push({from: 'lastname', to: 'last_name'});
      mapping.push({from: 'postalcode', to: 'postal_code'});
    }

    mapping.forEach((field) => {
      if (formData.get(field.from)) {
        formData.set(field.to, formData.get(field.from));
        formData.delete(field.from);
      }
    });
  }

  return formData;
};

const formDataToObject = (formData) => {
  const data = {};

  for (const fieldName of formData.keys()) {
    const value = formData.get(fieldName);
    data[fieldName] = value === 'on' ? '1' : value;
  }

  return data;
};

const getMetaConversionApiConsent = () => {
  const prefix = 'ae_cookie_consent_';

  return Cookies.get(`${prefix}meta_conversion_api`) === 'true' || Cookies.get(`${prefix}marketing`) === 'true'
    ? {consent_meta_conversion_api: true}
    : {};
};

const getMetaClickId = () => {
  const fbc = Cookies.get('_fbc');

  return fbc ? {fbc} : {};
};

const getMetaBrowserId = () => {
  const fbp = Cookies.get('_fbp');

  return fbp ? {fbp} : {};
};

const recaptchaSiteKey = () => window?.aeBlockPetitionConfig?.recaptchaKeyV3;

const dispatchEvent = (system, status, data) => {
  if (isTypeSignature(system)) {
    document.dispatchEvent(
      new window.CustomEvent('petition', {
        detail: {
          type: 'signature',
          status,
          data
        }
      })
    );
  } else if (isTypeSubscription(system)) {
    document.dispatchEvent(
      new window.CustomEvent('listo', {
        detail: {
          type: 'subscription',
          status,
          data
        }
      })
    );
  }
};

async function submit(form, data, recaptchaToken = null) {
  const url = form.getAttribute('action');

  if (!url) {
    throw Error('cygne is not configured');
  }

  if (recaptchaToken) {
    data.recaptcha_token = recaptchaToken;
  }

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({
      ...data,
      ...getUtmParameterObject(window.location.search, data),
      ...getMetaConversionApiConsent(),
      ...getMetaClickId(),
      ...getMetaBrowserId()
    }),
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json; charset=utf-8'
    }
  });

  const json = await response.json();

  if (response.status < 200 || response.status >= 300) {
    throw Error(`AJAX request returned code ${response.status}`, {cause: json.errors});
  }

  if (!['accepted', 'ok'].includes(json.status)) {
    throw Error('Backend rejected the submission', {cause: json.errors});
  }

  return Promise.resolve();
}

export {
  filterFormData,
  formDataToObject,
  recaptchaSiteKey,
  dispatchEvent,
  submit as submitForm
};
