const utmParameterNames = ['utm_campaign', 'utm_medium', 'utm_source', 'utm_content', 'utm_term', 'utm_tag'];

const getUtmParameterObject = (queryString, data) => {
  const queryParameters = new URLSearchParams(queryString);
  const utmParameters = {};

  for (const utmParameterName of utmParameterNames) {
    if (queryParameters.has(utmParameterName)) {
      utmParameters[utmParameterName] = queryParameters.get(utmParameterName);
    } else if (data && data[utmParameterName]) {
      utmParameters[utmParameterName] = data[utmParameterName];
    }
  }

  return utmParameters;
};

export {getUtmParameterObject};
