const PETITION_LOCALSTORAGE_KEY = 'ae.petition.userdata';

const snakelize = (key) => key ? key.split(/(?=[A-Z])/).join('_').toLowerCase() : key;

/**
 * Stores data in the Local Storage
 *
 * @param userData The user data (object) to store
 * @param key Optional local storage key
 */
function save(userData, key = PETITION_LOCALSTORAGE_KEY) {
  if (typeof (Storage) !== 'undefined') {
    window.localStorage.setItem(key, JSON.stringify(userData));
  }
}

/**
 * Restores the user data from Local Storage
 *
 * @param key Optional local storage key
 */
function load(form, key = PETITION_LOCALSTORAGE_KEY) {
  if (typeof (Storage) === 'undefined') return;

  const userData = JSON.parse(window.localStorage.getItem(key));

  if (userData) {
    const changeEvent = new Event('change');
    let field;

    for (const fieldName of ['lastname', 'firstname', 'country', 'city', 'postalcode', 'email']) {
      field = form.querySelector(`[name=${fieldName}]`);

      if (!field) {
        continue;
      }

      if (userData[fieldName] !== undefined) {
        field.value = userData[fieldName];
      } else if (userData[snakelize(fieldName)] !== undefined) {
        // Use snakelize'd localStorage for camelCase'd fields name.
        field.value = userData[snakelize(fieldName)];
      }

      field.dispatchEvent(changeEvent);
    }
  }
}

export {save, load};
