import {_x} from '@wordpress/i18n';

/*
 * Possible operations:
 * - tld: substitute the TLD
 * - domain: substitute the whole domain part (including the TLD)
 * - ignore: email is invalid
 * - sub: custom substitution
 */

// symfony/validator PHP regex:
// /^[\w.!#$%&\'*+\\/=?^_`{|}~-]+@\w(?:[\w-]{0,61}\w)?(?:\.\w(?:[\w-]{0,61}\w)?)+$/
const VALIDATION_REGEXP = /^\w([\w.+-]*[\w-])?@([\w-]+\.)+[a-z]{2,5}$/i;

const fixes = {
  // fix common .com tld typo
  '@(hotmail|gmail|msn|yahoo|icloud|outlook|aol|btinternet)\\.(con|conm|cim|cin|cm|om|coom|om|ocm|cpm|clm|ccom|coo|[fdvx]om|col|com[cealmnopiry]|ckm|vcom|co\\.com)$': {
    tld: 'com'
  },
  // fix common .com tld typo
  '@(icloud|gmail)\\.(es|it|fr|co|br|com?\\.uk|com\\.(br|mx|ar))$': {
    tld: 'com'
  },
  // No known-use of the columbian-suffix for these providers
  '@(hotmail|gmail)\\.(co|c|con|von|vom|com.)$': {
    tld: 'com'
  },
  // fix common .fr tld typo
  '@(neuf|wanadoo|sfr|orange|bbox|free)\\.(de|ft|fe)$': {
    tld: 'fr'
  },
  // another set of typo for domains (for which .de is a valid tld)
  '@(hotmail|outlook|live)\\.(ft|fe)$': {
    tld: 'fr'
  },
  // web.de
  '@(eeb|wen|wwb)\\.(de)$': {
    dom_no_ext: 'web'
  },
  // gmx.de
  '@(gnx|gmc|hmx)\\.(de)$': {
    dom_no_ext: 'gmx'
  },
  // googlemail.com
  '@(googlemsil|goolemail|goooglemail|googlmail|googemail|googleail|googlwmail)\\.(com)$': {
    dom_no_ext: 'googlemail'
  },
  // UK tld fix
  '.*\\.(couk|comuk|ci\\.uk|co\\.(yk|u[jl]|ik|jk|uik|uk[ijkoprtul]?))$': {
    tld2: 'co.uk'
  },
  // BR tld fix
  '.*\\.(combr|vom\\.br)$': {
    tld2: 'com.br'
  },
  // icloud.com
  '@(iclaud|iclud|icluod|icoud|iclou|[ou]cloud|icloyd|icooud|icloude|cloud|iclod|icolud|iclould|ickoud|iclpud)\\.com$': {
    dom_no_ext: 'icloud'
  },
  // fix outlook.com typo
  '.*@(oultlook|outhook|outlok|outloock|outloook|oulook|autlook|outollk|oitlook|oltlook|ourlook|outilook|outllo|outllok|outllook|outloo[lmokj]?)\\.(fr|com|be|es|jp|dk|pt|ie|it|co|co\\.uk|com\\.br)$': {
    dom_no_ext: 'outlook'
  },
  // fix hotmail.com typo
  // cc-tld at https://www.internetearnings.com/how-to-register-live-or-hotmail-e-mail-address/
  '.*@(homail|hoymail|hotmqil|hotamail|hotma[uol]l|hotmai[osmk]?|hotm[ai]l|hormail|hotmial|hot[pnl]?ail|hootmail|hitmail|hotamil|ghotmail|hotmaill|otmail|hotm[se]il|htmail|hirmail|hotimail|hornai|[jb]otmail)\\.(fr|es|de|es|com|it|co|be|ch|ca|gr|fi|co\\.uk|com\\.co|com\\.br)$': {
    dom_no_ext: 'hotmail'
  },
  // fix btinternet typo
  '.*@(btibternet|byinternet|brinternet|btinyernet|btimternet|btinterbet|btinter.com|btinter\\.et|btinterne|btinterney|btinternrt|btinternt|btinterrnet|btintwrnet|btinyernet|btingernet|btinterner)\\.com$': {
    domain: 'btinternet.com'
  },
  // fix gmail.com typo
  '.*@(hmail|g-mail|gail|gamil|gma[uo]?l|gmil|gmial|gmsil|gpail|g[iean]mail|gnail|gmail[lm]|gmai[ikmp]?|glail|gimal|gimail|gmqil|gmali|gmaiil|gmaial|gmeil|gmiail|gem[ea]il|gmaiol|gmaeil|gamal|g\\.mail)\\.(com|co|c|con|von|vom|co[mn]\\.br|com.)$': {
    domain: 'gmail.com'
  },
  // fix libero.it national tld
  '.*@(ibero\\.it|l[ou]?bero\\.it|libet?o\\.it|libero\\.(ir|ti|ut|il|ot|iy|ig)|liber\\.it|libr?ro\\.it|li[nv]ero\\.it|liber[ai]\\.it)$': {
    domain: 'libero.it'
  },
  '.*@(tiscal|tiscsli|ctiscali|t[iu]scali)\\.(com?|i[tfilry]|itl|co\\.ut)$': {
    domain: 'tiscali.it'
  },
  '.*@(tiscali?|tiscsli|ctiscali|t[iu]scali)\\.co\\.[iu]k': {
    domain: 'tiscali.co.uk'
  },
  // fix gmail.com national tld
  '.*@gmail\\.(fr|nl|es|net|com\\.com|com\\.br|con\\.br)$': {
    domain: 'gmail.com'
  },
  // Comcast
  '.*@comast\\.net$': {
    domain: 'comcast.net'
  },
  // fix free.fr typo
  '.*@(free\\.fe|fre\\.fr|frer\\.fr)$': {
    domain: 'free.fr'
  },
  // fix aol.com typo
  // Warn: uol.com.br is a valid provider
  '.*@(ail|aol|apl|alo|aoo|uol)\\.(com?|co\\.uk)$': {
    dom_no_ext: 'aol'
  },
  // fix yahoo.co.uk typo
  '.*@yaho.co.uk': {
    domain: 'yahoo.co.uk'
  },
  // fix yahootypo
  '.*@(uahoo|yahoi|yahho|yshoo|yajoo|yhaoo|yaboo|yahhoo|yaho|yhoo|yaoo|yanoo|tahoo|yahooo)\\.(es|fr|com|it|no|co\\.uk|com\\.br)$': {
    dom_no_ext: 'yahoo'
  },
  // ignore trashable/temporary email address
  '.*@(yopmail\\.com|mailinator\\.com|facebook\\.com|spamgourmet\\.com|tinfoil-fake-site\\.com|byom\\.de|mvrht\\.net)$': {
    ignore: true
  },
  // change "abc.def.@provider.com" INTO "abc.def@provider.com"
  '^(.*)\\.+@(.*)$': {
    sub: '$1@$2'
  }
};

function fix(email, options = {}) {
  if (!email) {
    return email;
  }

  for (const [pattern, operations] of Object.entries(fixes)) {
    const regex = new RegExp(pattern);

    if (!email.match(regex)) { // not concerned by this filter
      continue;
    }

    for (const [operation, value] of Object.entries(operations)) {
      switch (operation) {
        case 'tld':
          email = email.replace(/\.[^.]+$/, '.' + value);
          break;
        case 'tld2':
          email = email.replace(/\.([^.]+\.[^.]+)$/, '.' + value);
          break;
        case 'domain':
          email = email.replace(/@(.*)$/, '@' + value);
          break;
        case 'dom_no_ext':
          email = email.replace(/@.*?\.(.+)$/, '@' + value + '.' + '$1');
          break;
        case 'sub':
          email = email.replace(regex, value);
          break;
        case 'ignore':
          return value ? null : email;
      }
    }
  }

  return options.lowercase ? email.toLowerCase() : email;
};

function validate(field) {
  const email = field.value;
  const emailFixed = fix(email);

  if (VALIDATION_REGEXP.test(email) && !email.includes('..') && emailFixed === email) {
    field.setCustomValidity('');
  } else if (emailFixed !== email) {
    field.setCustomValidity(`${_x('Email seems mistyped. Suggested:', 'email suggestion text', 'ae-block-petition')} ${emailFixed}`);
  } else {
    field.setCustomValidity(_x('Invalid email', 'invalid email error', 'ae-block-petition'));
  }
}

export {validate, fix, VALIDATION_REGEXP};
