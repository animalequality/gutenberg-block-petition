import exampleImage from '../../assets/images/replace/1000x1000.jpg';

const getInnerFieldBlocks = (block, fields = []) => {
  block && block.innerBlocks.forEach(innerBlock => {
    if (innerBlock.innerBlocks.length || !innerBlock.name.includes('ae-petition/field-')) {
      return getInnerFieldBlocks(innerBlock, fields);
    }

    if (innerBlock.name.includes('ae-petition/field-') && innerBlock.name !== 'ae-petition/field-submit') {
      fields.push(innerBlock);
    }

    return fields;
  });

  return fields;
};

const getBlockIndexOfSibling = (select, clientId) => {
  try {
    const parentBlocks = select('core/block-editor').getBlockParents(clientId);
    const directParent = select('core/block-editor').getBlocksByClientId(parentBlocks[parentBlocks.length - 1])[0];

    for (const [key, sibling] of directParent.innerBlocks.entries()) {
      if (sibling.clientId === clientId) {
        return (key + 1);
      }
    }
  } catch (error) {
    return '';
  }
};

const getMajorityFieldsRequiredStatus = (select, blockClientId) => {
  const form = select('core/block-editor').getBlockParentsByBlockName(blockClientId, 'ae-petition/form');

  if (form) {
    const fields = getInnerFieldBlocks(select('core/block-editor').getBlocksByClientId(form)[0]);
    const requiredFields = fields.filter(field => field.attributes.required);

    return requiredFields.length > fields.length / 2 && requiredFields.length < fields.length;
  }

  return false;
};

const getMultiStepTemplate = (rightColumnContent, rightColumnAllowedInnerBlocks) => [
  [
    'core/columns', {
      templateLock: 'insert'
    }, [
      [
        'core/column', {
          lock: {remove: true, move: false},
          allowedBlocks: ['core/image']
        }, [
          [
            'core/image', {
              url: exampleImage,
              lock: {remove: true, move: true}
            }
          ]
        ]
      ], [
        'core/column', {
          templateLock: false,
          lock: {remove: true, move: false},
          allowedBlocks: ['core/group']
        }, [['core/group', {
          allowedBlocks: rightColumnAllowedInnerBlocks
        }, rightColumnContent]]
      ]
    ]
  ]
];

export {getBlockIndexOfSibling, getMajorityFieldsRequiredStatus, getMultiStepTemplate};
