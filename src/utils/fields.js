import {Fragment} from '@wordpress/element';
import {_x} from '@wordpress/i18n';
import icon from '../../block-icon.js';

const createVariation = (name, label, description, isDefault = false, attributes = {}) => {
  return {
    name,
    title: label,
    description,
    isDefault,
    icon,
    attributes: {
      name,
      label,
      ...attributes
    }
  };
};

const selectOptionsToTextarea = options => {
  return options.length > 0 ? options.map(option => `${option.value}:${option.label}`).join('\n') : '';
};

const textareaToSelectOptions = (text, addEmptyOption = false) => {
  if (!text) {
    return [];
  }

  const options = text.split('\n').map(line => {
    const [value, label] = line.split(':');
    return value && label ? {value, label} : null;
  }).filter(option => !!option);

  if (addEmptyOption) {
    options.unshift({label: _x('Please select', 'unselected dropdown label', 'ae-block-petition'), value: ''});
  }

  return options;
};

const renderField = (type, name, label, isRequired, useOptional, validationPattern = null, options = [], defaultOption = null, addEmptyOption = false) => {
  let input = null;
  let fieldRequiredIndicator = '';
  const commonAttributes = {
    id: name,
    name,
    required: isRequired
  };

  if (type === 'select') {
    input = <select {...commonAttributes}>
      {
        (addEmptyOption ? [{label: _x('Please select', 'unselected dropdown label', 'ae-block-petition'), value: ''}] : []).concat(options).map((option, index) => <option key={index} value={option.value} selected={option.value === defaultOption}>
          {option.label}
        </option>)
      }
    </select>;
  } else {
    input = <input type={type} {...commonAttributes} pattern={validationPattern || null} />;
  }

  if (isRequired && !useOptional) {
    fieldRequiredIndicator = '<span>*</span>';
  } else if (!isRequired && useOptional) {
    fieldRequiredIndicator = `<span> (${_x('optional', 'optional field hint', 'ae-block-petition')})</span>`;
  }

  return <Fragment>
    {type === 'checkbox' && input}

    <label htmlFor={name} dangerouslySetInnerHTML={{__html: label + fieldRequiredIndicator}}></label>

    {type !== 'checkbox' && input}
  </Fragment>;
};

export {createVariation, renderField, selectOptionsToTextarea, textareaToSelectOptions};
