import Modal from 'bootstrap/js/dist/modal.js';
import {recursivelyReplaceHtml} from '../utils/text.js';
import {getUtmParameterObject} from '../utils/url.js';

const deepLink = (modalElement, fragmentId) => {
  if (window.location.hash === `#${fragmentId}`) {
    show(modalElement);
  }
};

const appendUtmParametersToUrl = (url, utmParameters) => {
  try {
    const parsedUrl = new URL(url);

    for (const [name, value] of Object.entries(utmParameters)) {
      parsedUrl.searchParams.set(name, value);
    }

    return parsedUrl.toString();
  } catch (exception) {
    return url;
  }
};

const show = (modalElement, data = null, redirectToAfterClose = null, redirectToAfterCloseWithSubscription = null) => {
  if (!modalElement) {
    return;
  }

  if (data && (data.firstname || data.firstName)) {
    recursivelyReplaceHtml(modalElement, '{firstname}', data.firstname ? data.firstname : data.firstName);
  }

  const modal = new Modal(modalElement);
  const closeButton = modalElement.querySelector('.wp-block-button.close-modal a');

  if (closeButton) {
    closeButton.addEventListener('click', event => {
      event.preventDefault();
      modal.hide();
    });
  }

  if (redirectToAfterClose) {
    const utmParameters = getUtmParameterObject(window.location.search, data);

    modal._element.addEventListener('hidden.bs.modal', () => {
      window.location = appendUtmParametersToUrl(
        redirectToAfterCloseWithSubscription && data.newsletter === '1'
          ? redirectToAfterCloseWithSubscription
          : redirectToAfterClose,
        utmParameters
      );
    });
  }

  modal.show();
};

export {deepLink, show};
