import apiFetch from '@wordpress/api-fetch';
import {getUtmParameterObject} from './url.js';

async function submit(endpoint, data) {
  const postId = window.aeBlockPetitionConfig?.postId;
  if (!data.newsletter || !endpoint || !postId) {
    return Promise.resolve();
  }

  const response = await apiFetch({
    path: '/ae-block-petition/pardot',
    method: 'POST',
    parse: false,
    data: {post_id: postId, ...data, ...getUtmParameterObject(window.location.search, data)}
  });

  if (response.status < 200 || response.status >= 300) {
    throw Error(`Pardot AJAX request returned code ${response.status}`, {cause: {response, json: false}});
  }

  const json = await response.json();

  if (json.success === false) {
    throw Error(`Pardot AJAX request returned code ${response.status}`, {cause: {response, json: false}});
  }

  return Promise.resolve();
}

export {submit};
