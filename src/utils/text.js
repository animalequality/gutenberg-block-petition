const domTraversal = (parentElement, callback) => {
  parentElement.childNodes.forEach((element) => {
    callback(element);

    if (element.hasChildNodes()) {
      domTraversal(element, callback);
    }
  });
};

const recursivelyReplaceHtml = (rootNode, search, replace) => {
  domTraversal(rootNode, element => {
    if (element.nodeType === window.Node.TEXT_NODE) {
      element.nodeValue = element.nodeValue.replace(search, replace);
    }
  });
};

const firstCharacterUppercase = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export {firstCharacterUppercase, recursivelyReplaceHtml};
