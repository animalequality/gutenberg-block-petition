const CYGNE_URL = 'https://sign.animalequality.org/api/petitions/%d/signatures';
const LISTO_URL = 'https://listo.animalequality.org/api/subscribe';
// TODO check which ones we don't need anymore
const FIELDS_TO_FILTER_REQUEST = [
  '__form-name__', '__unique_form_id__', 'form-nonce',
  // These two are only used for non-XHR submission (plain form submission)
  'redirect_error', 'redirect_success',
  'scrollto', 'honeypot', 'privacy'
];
const FIELDS_TO_FILTER_STORAGE = FIELDS_TO_FILTER_REQUEST.concat(['recaptcha_token', 'newsletter', 'privacy']);

const getCountryList = () => {
  return [
    {label: 'BR', value: 'br'},
    {label: 'DE', value: 'de'},
    {label: 'ES', value: 'es'},
    {label: 'IN', value: 'in'},
    {label: 'IT', value: 'it'},
    {label: 'MX', value: 'mx'},
    {label: 'UK', value: 'uk'},
    {label: 'US', value: 'us'}
  ];
};

const isTypeSubscription = system => {
  return system === 'listo';
};

const isTypeSignature = system => {
  return system !== 'listo';
};

const isSystemUsingCygne = system => {
  return system.includes('cygne');
};

const isSystemUsingOwnForm = system => {
  return system !== 'everyaction';
};

const getFormAction = (country, system, petitionId) => {
  if (system === 'salesforce' || system === 'listo') {
    return `${LISTO_URL}/sf${country}?groups=${petitionId}`;
  } else if (isSystemUsingCygne(system)) {
    return CYGNE_URL.replace(/%d/, petitionId);
  } else if (system === 'url') {
    return petitionId;
  }

  return null;
};

const getFromGlobalConfig = (name) => {
  return window.aeBlockPetitionConfig[name];
};

export {
  CYGNE_URL,
  FIELDS_TO_FILTER_REQUEST,
  FIELDS_TO_FILTER_STORAGE,
  getCountryList,
  isSystemUsingCygne,
  isTypeSignature,
  isTypeSubscription,
  isSystemUsingOwnForm,
  getFormAction,
  getFromGlobalConfig
};
