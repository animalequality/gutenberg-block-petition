import {useBlockProps} from '@wordpress/block-editor';
import {renderField} from '../../utils/fields.js';

export default (type) => {
  return ({attributes: {name, label, required, validationPattern, validationEmail, useOptional, options, defaultOption, addEmptyOption}}) => (
    <div {...useBlockProps.save({className: `form-group ${type === 'checkbox' ? 'form-group--checkbox' : ''}`})}>
      {renderField(type, name, label, required, useOptional, validationPattern, options, defaultOption, addEmptyOption)}
    </div>
  );
};
