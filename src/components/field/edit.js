import {Fragment, useEffect, useState, useRef} from '@wordpress/element';
import {InspectorControls, useBlockProps} from '@wordpress/block-editor';
import {TextControl, TextareaControl, CheckboxControl, Panel, PanelBody, PanelRow, Button} from '@wordpress/components';
import {useSelect} from '@wordpress/data';
import {renderField, textareaToSelectOptions, selectOptionsToTextarea} from '../../utils/fields.js';
import {getMajorityFieldsRequiredStatus} from '../../utils/blocks.js';
import {countries} from 'country-data-list';

export default (type) => {
  return ({clientId, attributes: {name, label, required, validationPattern, validationEmail, options, defaultOption, addEmptyOption}, setAttributes}) => {
    const [optionsText, setOptionsText] = useState(options ? selectOptionsToTextarea(options) : null);
    const [lockFieldNameClicks, setLockFieldNameClicks] = useState(0);
    const initialFieldName = useRef(name);
    const lockFieldNameTimeout = useRef(null);

    const majorityRequired = useSelect(select => {
      return getMajorityFieldsRequiredStatus(select, clientId);
    }, [required]);

    const unlockNameField = event => {
      let lockFieldNameClicksLocal = lockFieldNameClicks;

      event.preventDefault();
      lockFieldNameClicksLocal++;
      setLockFieldNameClicks(lockFieldNameClicksLocal);
      window.clearTimeout(lockFieldNameTimeout.current);

      lockFieldNameTimeout.current = window.setTimeout(() => {
        setLockFieldNameClicks(0);
      }, 2000);

      if (lockFieldNameClicksLocal > 2) {
        window.clearTimeout(lockFieldNameTimeout.current);
      }
    };

    useEffect(() => {
      setAttributes({useOptional: majorityRequired});
    }, [majorityRequired]);

    useEffect(() => {
      if (type === 'select' && name === 'country' && options.length === 0) {
        const countriesToUse = countries.all
          .filter(country => !['AC', 'DD'].includes(country.alpha2))
          .map(country => { return {value: country.alpha2, label: country.name}; })
          .sort((a, b) => a.label.localeCompare(b.label));

        setAttributes({options: countriesToUse});
        setOptionsText(selectOptionsToTextarea(countriesToUse));
      }
    }, []);

    return (
      <Fragment>
        <InspectorControls>
          <Panel>
            <PanelBody title='Field Settings' initialOpen={true}>
              <PanelRow>
                <div onClick={unlockNameField}>
                  <TextControl
                    label='Technical Name'
                    value={name}
                    help='The petition system relies on certain field names. Therefore only change if you know what you are doing. I case of doubt, contact the TECH team. Click here 3 times to change the value.'
                    onChange={name => setAttributes({name})}
                    disabled={initialFieldName.current && lockFieldNameClicks <= 2}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <TextControl
                  label='Label'
                  value={label}
                  onChange={label => setAttributes({label})}
                />
              </PanelRow>
              <PanelRow>
                <CheckboxControl
                  label='Required'
                  checked={required}
                  onChange={required => setAttributes({required})}
                />
              </PanelRow>
            </PanelBody>
          </Panel>

          {
            type === 'select' &&
            <Panel>
              <PanelBody title='Select Options Settings' initialOpen={true}>
                <PanelRow>
                  <CheckboxControl
                    label='Add empty option'
                    help='Adds an empty option to the top which says "Please select"'
                    checked={addEmptyOption}
                    onChange={addEmptyOption => setAttributes({addEmptyOption})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextControl
                    label='Default Option'
                    help='If this is empty, the first option from the options will be selected by default'
                    value={defaultOption}
                    onChange={defaultOption => setAttributes({defaultOption})}
                  />
                </PanelRow>
                <PanelRow>
                  <TextareaControl
                    label='Options'
                    help='Provide one option per line in the format value:label. The options are rendered in the same order as provided here.'
                    value={optionsText}
                    onChange={optionsText => setOptionsText(optionsText)}
                  />
                </PanelRow>
                <PanelRow>
                  <Button variant='secondary' onClick={() => setAttributes({options: textareaToSelectOptions(optionsText)})}>
                    Save options
                  </Button>
                </PanelRow>
              </PanelBody>
            </Panel>
          }

          {
            type === 'text' &&
            <Panel>
              <PanelBody title='Validation Settings' initialOpen={true}>
                <PanelRow>
                  <TextControl
                    label='Pattern'
                    help='Provide a regex pattern which is used to validate the field. This should only be changed by TECH.'
                    value={validationPattern}
                    onChange={validationPattern => setAttributes({validationPattern})}
                  />
                </PanelRow>
              </PanelBody>
            </Panel>
          }
        </InspectorControls>

        <div {...useBlockProps({className: `form-group ${type === 'checkbox' ? 'form-group--checkbox' : ''}`})}>
          {renderField(type, name, label, required, majorityRequired, validationPattern, options, defaultOption)}
        </div>
      </Fragment>
    );
  };
};
