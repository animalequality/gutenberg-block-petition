import {_x} from '@wordpress/i18n';

export default [
  {label: 'Bill', value: 'bill'},
  {label: 'Success', value: 'success'},
  {label: 'Error', value: 'error'}
];
