import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';

export default () => (
  <div {...useBlockProps.save()} id='ae-petition'>
    <InnerBlocks.Content />
  </div>
);
