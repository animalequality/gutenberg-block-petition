import ready from 'document-ready';
import load from 'load-script';
import {_x} from '@wordpress/i18n';
import {filterFormData, formDataToObject, recaptchaSiteKey, submitForm, dispatchEvent} from '../utils/form.js';
import {load as localStorageLoad, save as localStorageSave} from '../utils/localStorage.js';
import {show as showModal, deepLink} from '../utils/modal.js';
import {event} from '../utils/tracking.js';
import {submit as submitPardot} from '../utils/pardot.js';
import {FIELDS_TO_FILTER_REQUEST, FIELDS_TO_FILTER_STORAGE, isSystemUsingCygne, isSystemUsingOwnForm, getFromGlobalConfig} from '../utils/config.js';

const everyactionSubmitHandler = (successModalElement) => {
  window.nvtag_callbacks = window.nvtag_callbacks || {};
  window.nvtag_callbacks.preSegue = window.nvtag_callbacks.preSegue || [];

  if (successModalElement) {
    window.nvtag_callbacks.preSegue.push((formData) => {
      showModal(
        successModalElement,
        {firstname: formData?.postVals?.FirstName},
        getFromGlobalConfig('successRedirectAfterModalUrl'),
        getFromGlobalConfig('successRedirectAfterModalWithSubscriptionUrl')
      );
    });
  }
};

const setSubmitButtonLoadingState = (form, isLoading) => {
  const button = form.querySelector('button');

  if (button) {
    form.querySelector('button').classList[isLoading ? 'add' : 'remove']('btn--pending');
  }
};

const getPetitionBlockElements = (petitionBlock) => {
  const formBlock = petitionBlock.querySelector('.wp-block-ae-petition-form');

  return {
    formBlock,
    form: formBlock.querySelector('form'),
    link: petitionBlock.querySelector('.wp-block-ae-petition-bill-link a'),
    loadingOverlay: petitionBlock.querySelector('.loading-overlay'),
    config: {
      system: formBlock.dataset.system,
      petitionSlug: formBlock.dataset.petitionSlug,
      pardotEndpoint: formBlock.dataset.pardotEndpoint,
      preventAutofill: formBlock.dataset.preventAutofill === 'true',
      enableRecaptcha: formBlock.dataset.enableRecaptcha === 'true',
      recaptchaPosition: formBlock.dataset.recaptchaPosition,
      modalSuccessElement: petitionBlock.querySelector('.modal.petition-modal-success'),
      modalErrorElement: petitionBlock.querySelector('.modal.petition-modal-error'),
      thankYouPageUrl: formBlock.dataset.thankYouPageUrl
    }
  };
};

const submit = async(form, {system, petitionSlug, preventAutofill, pardotEndpoint, modalSuccessElement, modalErrorElement, thankYouPageUrl}, recaptchaToken = null) => {
  try {
    const data = formDataToObject(filterFormData(form, FIELDS_TO_FILTER_REQUEST, isSystemUsingCygne(system) ? 'cygne' : 'salesforce'));
    const hasNewsletter = !!data.newsletter;

    data.eventId = Math.floor(Math.random() * 1000000000);

    if (getFromGlobalConfig('shouldSendFormUrl')) {
      data.formUrl = window.location.origin + window.location.pathname;
    }

    const trackingData = {...data, newsletterAccepted: hasNewsletter, petitionSlug};

    dispatchEvent(system, 'submit', data);

    const [formSubmission, pardotSubmission] = await Promise.allSettled([
      submitForm(form, data, recaptchaToken),
      submitPardot(pardotEndpoint, data)
    ]);

    if (formSubmission.status === 'rejected') {
      throw formSubmission.reason;
    }
    if (pardotSubmission.status === 'rejected') {
      throw pardotSubmission.reason;
    }

    if (!preventAutofill) {
      localStorageSave(formDataToObject(filterFormData(form, FIELDS_TO_FILTER_STORAGE)));
    }

    if (hasNewsletter) {
      event('newsletter:success', {eventAction: 'subscribe', ...trackingData});
    }

    event('petition:success', {eventAction: 'sign', ...trackingData});

    dispatchEvent(system, 'success', data);

    if (thankYouPageUrl) {
      window.location = thankYouPageUrl;
    } else {
      showModal(
        modalSuccessElement,
        data,
        getFromGlobalConfig('successRedirectAfterModalUrl'),
        getFromGlobalConfig('successRedirectAfterModalWithSubscriptionUrl')
      );
    }

    form.reset();
    setSubmitButtonLoadingState(form, false);
  } catch (error) {
    console.warn('Submission failed', error);
    let errorMessage = null;

    if (error && error.details && error.details.filter(message => message.includes('reCaptcha').length > 0)) {
      errorMessage = _x('Recaptcha validation failed', 'recaptcha validation error message', 'ae-block-petition');
    }

    dispatchEvent(system, 'failed', error);
    showModal(modalErrorElement, errorMessage);
    setSubmitButtonLoadingState(form, false);
  }
};

const formSubmitHandler = async(event, config, enableRecaptcha = false) => {
  const form = event.target;

  event.preventDefault();
  setSubmitButtonLoadingState(form, true);

  if (enableRecaptcha && recaptchaSiteKey()) {
    const token = await window.grecaptcha.execute(getFromGlobalConfig('recaptchaKeyV3'), {action: 'submit'});

    if (token) {
      submit(form, config, token);
    }
  } else {
    submit(form, config);
  }
};

ready(async() => {
  const petitionBlocks = document.querySelectorAll('.wp-block-ae-petition-petition');
  const petitionBlocksWithoutRecaptcha = [...petitionBlocks].filter(petitionBlock => {
    const {config} = getPetitionBlockElements(petitionBlock);
    return !config.enableRecaptcha;
  });
  const petitionBlocksWithRecaptcha = [...petitionBlocks].filter(petitionBlock => {
    const {config} = getPetitionBlockElements(petitionBlock);
    return config.enableRecaptcha;
  });

  petitionBlocks.forEach(petitionBlock => {
    const {form, link, config} = getPetitionBlockElements(petitionBlock);

    if (link) {
      link.addEventListener('click', event => {
        event.preventDefault();
        showModal(petitionBlock.querySelector('.modal.petition-modal-bill'));
      });
    }

    deepLink(petitionBlock.querySelector('.modal.petition-modal-success'), 'petition-thank-you');
    deepLink(petitionBlock.querySelector('.modal.petition-modal-bill'), 'petition-text');

    if (config.system === 'everyaction') {
      everyactionSubmitHandler(petitionBlock.querySelector('.modal.petition-modal-success'));
      return;
    }

    if (!form) {
      return;
    }

    if (!config.preventAutofill) {
      localStorageLoad(form);
    }
  });

  if (petitionBlocksWithRecaptcha.length > 0 && recaptchaSiteKey()) {
    const badgePosition = getPetitionBlockElements(petitionBlocksWithRecaptcha[0])?.config?.recaptchaPosition;

    window.aeBlockPetitionOnLoadRecaptcha = () => {
      window.grecaptcha.ready(async() => {
        petitionBlocksWithRecaptcha.forEach(petitionBlock => {
          const {form, loadingOverlay, config} = getPetitionBlockElements(petitionBlock);

          if (isSystemUsingOwnForm(config.system)) {
            form.addEventListener('submit', event => formSubmitHandler(event, config, true));
            loadingOverlay.remove();
          }
        });
      });
    };

    load(`https://www.google.com/recaptcha/api.js?render=${recaptchaSiteKey()}&onload=aeBlockPetitionOnLoadRecaptcha${badgePosition ? `&badge=${badgePosition}` : ''}`);
  }

  if (petitionBlocksWithoutRecaptcha.length > 0) {
    petitionBlocksWithoutRecaptcha.forEach(petitionBlock => {
      const {form, loadingOverlay, config} = getPetitionBlockElements(petitionBlock);

      if (isSystemUsingOwnForm(config.system)) {
        form.addEventListener('submit', event => formSubmitHandler(event, config));
        loadingOverlay.remove();
      }
    });
  }
});
