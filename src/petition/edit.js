import {InnerBlocks} from '@wordpress/block-editor';
import {_x} from '@wordpress/i18n';

export default () => {
  return (
    <InnerBlocks
      allowedBlocks={[
        'core/paragraph',
        'core/heading',
        'ae-petition/form',
        'ae-petition/modal-simple',
        'ae-petition/modal-multi-step',
        'ae-petition/bill-link',
        'animalequality/progress'
      ]}
      templateLock={false}
      template={[
        ['ae-petition/modal-simple', {trigger: 'bill'}],
        ['ae-petition/modal-multi-step', {
          lock: {move: false, remove: true},
          trigger: 'success'
        }],
        ['ae-petition/modal-simple', {
          lock: {move: false, remove: true},
          trigger: 'error'
        }, [
          [
            'core/paragraph', {
              textAlign: 'center',
              content: _x('An error occured, please try again later', 'default error text', 'ae-block-petition')
            }
          ]
        ]],
        ['core/paragraph', {
          content: 'Text to provide short introduction to the petition and explain the user why they should sign.',
          fontSize: 'medium',
          style: {
            typography: {fontStyle: 'italic'}
          }
        }],
        ['ae-petition/bill-link'],
        ['animalequality/progress'],
        ['ae-petition/form', {
          lock: {move: false, remove: true}
        }]
      ]}
    />
  );
};
