import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';
import {_x} from '@wordpress/i18n';

export default () => {
  return <div {...useBlockProps()}>
    <InnerBlocks
      allowedBlocks={['core/paragraph']}
      template={[
        [
          'core/paragraph', {
            content: `<a>${_x('Read the petition', 'default label for petition modal link', 'ae-block-petition')}</a>`
          }
        ]
      ]}
    />
  </div>;
};
