��          �   %   �      p     q     �     �  1   �  ;     $   =     b          �     �     �     �          *     D     d     �     �     �  7   �  6     O   >  :   �  !   �     �  >     '   G  "  o     �     �     �     �     	     	     1	     F	     M	     S	     Z	     _	  
   g	     r	     {	     	     �	  	   �	     �	     �	  '   �	     �	  :   �	     0
  	   A
  #   K
  	   o
                                                                                    	                            
           Animal Equality Petition Block Block to create a petition form Robin Schülein, Raphaël Droz button label for google loginSign in with Google default error textAn error occured, please try again later default field labelAnimal Defenders default field labelCheckbox default field labelCity default field labelCountry default field labelEmail default field labelFirst Name default field labelLast Name default field labelNewsletter default field labelPhone default field labelPostal Code default field labelPrivacy default field labelSelect default field labelStreet default field labelText default label for petition modal linkRead the petition email suggestion textEmail seems mistyped. Suggested: error message if config is missing for google loginGoogle client ID is missing https://gitlab.com/animalequality/gutenberg-block-petition invalid email errorInvalid email optional field hintoptional recaptcha validation error messageRecaptcha validation failed unselected dropdown labelPlease select Project-Id-Version: Animal Equality Petition Block 1.0.3
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/gutenberg-block-petition
PO-Revision-Date: 2024-06-13 12:13+0000
Last-Translator: Robin Schülein <robins@animalequality.de>
Language-Team: Italian <https://translate.animalequality.org/projects/ae/petition-gutenberg-blocks/it/>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15
X-Domain: ae-block-petition
 Animal Equality Petition Block Block to create a petition form Robin Schülein, Raphaël Droz Accedi con Google Errore, prova di nuovo Difensori degli animali Casella di controllo Città Paese E-mail Nome Cognome Notiziario Telefono CAP Privacy Selezionare Indirizzo Testo Leggi la petizione L'e-mail sembra digitata male, riprova: L'ID Google non esiste https://gitlab.com/animalequality/gutenberg-block-petition email non valida opzionale Convalida di Recaptcha non riuscita Seleziona 