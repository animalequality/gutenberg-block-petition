��          �   %   �      p     q     �     �  1   �  ;     $   =     b          �     �     �     �          *     D     d     �     �     �  7   �  6     O   >  :   �  !   �     �  >     '   G  +  o     �     �     �     �  3   	     H	     _	  	   d	     n	     t	     �	     �	     �	  	   �	     �	  
   �	     �	     �	     �	     �	  *   �	  !   %
  :   G
     �
     �
     �
     �
                                                                                    	                            
           Animal Equality Petition Block Block to create a petition form Robin Schülein, Raphaël Droz button label for google loginSign in with Google default error textAn error occured, please try again later default field labelAnimal Defenders default field labelCheckbox default field labelCity default field labelCountry default field labelEmail default field labelFirst Name default field labelLast Name default field labelNewsletter default field labelPhone default field labelPostal Code default field labelPrivacy default field labelSelect default field labelStreet default field labelText default label for petition modal linkRead the petition email suggestion textEmail seems mistyped. Suggested: error message if config is missing for google loginGoogle client ID is missing https://gitlab.com/animalequality/gutenberg-block-petition invalid email errorInvalid email optional field hintoptional recaptcha validation error messageRecaptcha validation failed unselected dropdown labelPlease select Project-Id-Version: AnimalEquality Gutenberg Block Petition 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/gutenberg-block-petition
PO-Revision-Date: 2024-06-13 12:13+0000
Last-Translator: Robin Schülein <robins@animalequality.de>
Language-Team: Spanish <https://translate.animalequality.org/projects/ae/petition-gutenberg-blocks/es/>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15
X-Domain: ae-block-petition
 Animal Equality Petition Block Block to create a petition form Robin Schülein, Raphaël Droz Iniciar sesión con Google Se produjo un error, inténtelo de nuevo más tarde Defensoras de animales Caja Localidad País Correo electrónico Nombre Apellido Boletin informativo Teléfono Código Postal Privacidad Seleccionar Calle Texto Leer la petición El email parece tener errores. Sugerencia: Falta la ID del cliente de Google https://gitlab.com/animalequality/gutenberg-block-petition Correo electrónico inválido opcional Validación de Recaptcha falló Por favor seleccione 