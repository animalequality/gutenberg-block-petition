/**
 * GPL v3, (c) Raphaël Droz 2022, raphael@droz.eu
 * $ trace-imports.js <source1> <dest1> [<sourceN> <destN>...]
 *
 * static analysis of JS imports to map source files to corresponding bundles.
 *
 * Parse each `source` JS-file specified on the command-line and recursively traverse its `import` statements.
 * <sourceN> are expected to be code base entrypoint (for webpack/babel)
 *
 * Then create a final JSON object whose keys are all the imported file name from sourceN
 *   and the values are the corresponding <destN>.
 *
 * Such a mapping is used by `wp i18n make-json --use-map` argument.
 **/

import fs from 'fs';
import path from 'path';
import process from 'process';
import {URL} from 'url';
import {parse} from '@babel/parser';
import traverse from '@babel/traverse';

const __filename = new URL('', import.meta.url).pathname;
let files = [];

function t(fullpath) {
  const dirname = path.dirname(fullpath);
  const filename = path.basename(fullpath).replace('.js', '') + '.js';
  const bck = process.cwd();
  process.chdir(dirname);
  // console.log("opening %s/%s", process.cwd(), filename);
  files.push(process.cwd() + '/' + filename);

  const data = fs.readFileSync(filename, 'utf-8');
  const ast = parse(data, {sourceType: 'module', plugins: ['jsx']});
  traverse.default(
    ast,
    {
      ImportDeclaration(apath, state) {
        const v = apath.get('source').node.value;
        if (!v.startsWith('./') && !v.startsWith('../')) {
          // console.log("[ignoring %s]", v);
          return;
        }
        if (v.match(/\.json$|\.(scss|jpg)/)) {
          return;
        }
        t(v);
      }
    }
  );

  process.chdir(bck);
}

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

function removeSelf(value) {
  return value.replace(process.cwd() + '/', '');
}

while (!process.argv[0].endsWith(path.basename(__filename))) {
  process.argv.shift();
}
process.argv.shift();

let allEntries = {};
while (process.argv.length >= 2) {
  const source = process.argv[0]; const dest = process.argv[1];
  t(source, process.cwd());

  allEntries = {...allEntries, ...Object.fromEntries(files.filter(onlyUnique).map(removeSelf).map(x => [x, dest]))};
  process.argv.shift();
  process.argv.shift();

  files = [];
}

console.log(JSON.stringify((allEntries)));
