# AnimalEquality Block Petition
Contributors: robink, drzraf\
Tags: ae, gutenberg, petition\
Requires at least: 5.0\
Tested up to: 6.0.1\
Stable tag: 2.4.1\
Requires PHP: 7.0\
License: GPLv3 or later\
License URI: https://www.gnu.org/licenses/gpl-3.0.html\
URI: https://gitlab.com/animalequality/gutenberg-block-petition
