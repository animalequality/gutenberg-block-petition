<?php

namespace AnimalEquality\WP;

class AEPardotRoute extends \WP_REST_Controller
{

    public function find_block(array $tests, $blockObject)
    {
        if (is_array($blockObject) && !isset($blockObject['blockName'])) {
            foreach ($blockObject as $b) {
                if ($r = $this->find_block($tests, $b)) {
                    return $r;
                }
            }
        }

        if (!$blockObject || empty($blockObject['blockName'])) {
            return false;
        }

        if ($tests[0]($blockObject)) {
            array_shift($tests);
            if (!$tests) {
                return $blockObject;
            }
            return $this->find_block($tests, $blockObject);
        }

        if ($blockObject['blockName'] === 'core/block' && is_numeric($blockObject['attrs']['ref'])) {
            return $this->find_block($tests, parse_blocks(get_post($blockObject['attrs']['ref'])->post_content));
        }

        if (!empty($blockObject['innerBlocks'])) {
            foreach ($blockObject['innerBlocks'] as $innerBlock) {
                if (!$innerBlock) {
                    continue;
                }
                $innerBlockObject = $this->find_block($tests, $innerBlock);
                if ($innerBlockObject) {
                    return $innerBlockObject;
                }
            }
        }
        return false;
    }

    // Store the result of this expensive post-parsing function into
    // a 5-min transient to optimize for speed in case of high-load.
    public function find_pardot_url_from_post(int $post_id)
    {
        $content = get_post($post_id)->post_content;
        $tests = [
            fn ($block) => $block['blockName'] === 'ae-petition/petition',
            fn ($block) => $block['blockName'] === 'ae-petition/form',
        ];

        $block = $this->find_block($tests, parse_blocks($content));
        $value = $block['attrs']['pardotEndpoint'] ?? false;
        set_transient('pardot_url_' . $post_id, $value, 5 * MINUTE_IN_SECONDS);
        return $value;
    }


    public function padot_missing_field($response)
    {
        return strpos(strtolower($response), 'field is required') !== false;
    }

    public function rest_validate_request_arg($value, $request, $param)
    {
        // if ($param == '...') { return new \WP_Error('rest_invalid_param', 'Foo', ['status' => 400]); }
        return rest_validate_request_arg($value, $request, $param);
    }

    public function post_to_pardot(\WP_REST_Request $request)
    {
        $data = $request->get_params();
        $post_id = $data['post_id'];

        if (! ($endpoint = get_transient('pardot_url_' . $post_id))) {
            $endpoint = $this->find_pardot_url_from_post($post_id);
            if (!wp_http_validate_url($endpoint)) {
                return new \WP_Error('rest_invalid_param', 'No valid Pardot endpoint', ['status' => 400]);
            }
        }

        $postFields = [
            'email',
            'firstName',
            'lastName',
            'country',
            'postalCode',
            'city',
            'utm_source',
            'utm_campaign',
            'utm_medium',
            'utm_term',
            'utm_content',
        ];

        $post = [];
        foreach ($postFields as $k) {
            if ($v = ($data[$k] ?? false)) {
                $post[$k] = $v;
            }
        }

        $response = wp_remote_post($endpoint, ['body' => $post]);

        if (
            !is_wp_error($response)
            && $response['response']['code'] === 200
            && !$this->padot_missing_field($response['body'])
        ) {
            return ['success' => true];
        }

        // warning: Passing on unfiltered Pardot response may leak sensitive private account/id/errors information.
        $message = $this->padot_missing_field($response['body'] ?? '') ? 'Pardot request missed fields' : $response['body'];

        return ['success' => false, 'message' => $message];
    }

    public function get_collection_params()
    {
        return [
            'context'  => $this->get_context_param(),
            'post_id' => [
                'type' => 'integer',
                'sanitize_callback' => 'absint',
                'validate_callback' => 'rest_validate_request_arg',
                'minimum'           => 1,
                'required'          => true,
            ],
            'email' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_email',
                'validate_callback' => 'is_email',
                'required'          => true,
            ],
            'firstName' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'lastName' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'country' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'postalCode' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'city' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'utm_source' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'utm_campaign' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'utm_medium' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'utm_term' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
            'utm_content' => [
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
                'validate_callback' => 'rest_validate_request_arg',
            ],
        ];
    }

    public function register_routes()
    {
        register_rest_route(
            'ae-block-petition',
            '/pardot',
            [
                [
                    'methods' => \WP_REST_Server::CREATABLE,
                    'args' => $this->get_collection_params(),
                    'callback' => [$this, 'post_to_pardot'],
                    'permission_callback' => '__return_true'
                ]
            ]
        );
    }
}
